Fiscal Date conversions
====

Installation
------------
fiscal_dates can be installed by running this in the terminal. It will be installed into the active Python environment.
```bash
$ pip install -U git+https://gitlab.com/advancedmd_data_team/fiscal_dates.git
```
Scripts were written using Python 3.6 and have not been tested for portability to Python 2. Necessary packages are:
 - Pandas (pandas==0.22.0)
 - Numpy (numpy==1.14.0)
 
Usage
----
AdvancedMD uses a macro called "GetDates" to get company specific months, years, weeks, and other fields from a date used for financial reporting. The `dates.py` python script contains a series of functions that return the same values as the VBA macro.
 - `get_half_year`
 - `get_week`
 - `get_sales_week`
 - `get_qtr`
 - `get_sales_qtr`
 - `get_sales_month`
 - `get_year`
 - `get_swim`

Here's an example of the `get_half_year` func and `get_sales_week` func on a single date value:
```python
>>> import pandas as pd
>>> from fiscal_dates import datesv  # datesv is the **fast** version
>>> test_df = pd.read_csv('./data/test_data.csv', parse_dates=True, infer_datetime_format=True)
>>> test_df['DATE'] = pd.to_datetime(test_df['DATE'])

>>> # Select a sample date
>>> d = test_df.loc[0, 'DATE']
>>> d
Timestamp('2016-12-31 00:00:00')

>>> # Half year func
>>> datesv.get_half_year(d)
2.0

>>> # Half year w/ fiscal context 
>>> datesv.get_half_year(d, get_fiscal=True)
1.0

>>> # Get the sales week
>>> datesv.get_sales_week(d)
27
```
Or creating a new column from the functions based on the dates
```python
>>> test_df['sales_week'] = datesv.get_sales_week(test_df['DATE'])
```

 
Questions?
----------
Please contact nathan.cheever@advancedmd.com :bowtie:


