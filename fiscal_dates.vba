# firstweekofyear params used in DatePart
# '0 = vbUseSystem - Use National Language Support (NLS) API setting
# '1 = vbFirstJan1 - Start with the week in which January 1 occurs (default)
# '2 = vbFirstFourDays - Start with the week that has at least four days in the new year
# '3 = vbFirstFullWeek - Start with the first full week of the new year


Public Function GetHalfYear(D1 As Date, GetFiscal As Boolean) As Integer
 
    Dim HalfYear As Integer
   
    If IsDate(D1) = False Then
        Exit Function
    End If
   
    HalfYear = Application.WorksheetFunction.RoundUp(Month(D1) / 6, 0)
   
    
    If GetFiscal = True Then
        If HalfYear = 1 Then
            HalfYear = 2
        Else
            HalfYear = 1
        End If
    End If
   
    GetHalfYear = HalfYear
   
End Function
 
Public Function GetQtr(D1 As Date, GetFiscal As Boolean) As Integer
    Dim Qtr As Integer
   
    
    If IsDate(D1) = False Then
        Exit Function
    End If
   
    Qtr = Application.WorksheetFunction.RoundUp(Month(D1) / 3, 0)
   
    If GetFiscal = True Then
        Qtr = Qtr - 2
       
        If Qtr <= 0 Then
            Qtr = Qtr + 4
        End If
           
    End If
       
    GetQtr = Qtr
 
End Function
 
Public Function GetWeek(D1 As Date, GetFiscal As Boolean) As Integer
    Dim Week As Integer
   
    If IsDate(D1) = False Then
        Exit Function
    End If
   
    Week = DatePart("ww", D1, vbSaturday, 2)
   
    If GetFiscal = True Then
        Week = Week - 26
       
        If Week <= 0 Then
            Week = Week + 52
        End If
   
    End If
   
    GetWeek = Week

   
End Function
Public Function GetSalesWeek(D1 As Date) As Integer
    Dim Week As Integer
   
    If IsDate(D1) = False Then
        Exit Function
    End If
   
    Week = DatePart("ww", D1, vbFriday, 0)
   
    
        Week = Week - 26
       
        If Week <= 0 Then
            Week = Week + 52
        End If
   
    If D1 >= #1/1/2016# Then
        Week = Week + 1
       
        If D1 > #6/30/2016# Then
            Week = Week - 1
        End If
    End If
       
    If D1 >= #1/1/2021# Then
        Week = Week + 1
       
        If D1 > #7/1/2021# Then
            Week = Week - 1
        End If
    End If
   
    If D1 >= #1/1/2027# Then
        Week = Week + 1
       
        If D1 > #7/1/2027# Then
            Week = Week - 1
        End If
    End If
   
   
    GetSalesWeek = Week
   
'0 = vbUseSystem - Use National Language Support (NLS) API setting
'1 = vbFirstJan1 - Start with the week in which January 1 occurs
'(default)
'2 = vbFirstFourDays - Start with the week that has at least four days
'in the
'new year
'3 = vbFirstFullWeek - Start with the first full week of the new year
   
End Function
 
 
Public Function GetSalesQtr(D1 As Date) As Integer
    Dim Qtr As Integer
    Dim Salesweek As Integer
   
    If IsDate(D1) = False Then
        Exit Function
    End If
   
    Salesweek = GetSalesWeek(D1)
   
    Qtr = Application.WorksheetFunction.RoundUp(Salesweek / 13, 0)
   
    GetSalesQtr = Qtr
 
End Function
Public Function GetSalesMonth(D1 As Date) As Double
    Dim SalesMonth As Integer
    Dim Salesweek As Integer
    Dim SalesweekValue As Double
   
    If IsDate(D1) = False Then
        Exit Function
    End If
   
    Salesweek = GetSalesWeek(D1)
   
    SalesweekValue = Salesweek / 13
   
    If SalesweekValue > 0 And SalesweekValue < 0.31 Then
        SalesMonth = 1
    End If
   
    If SalesweekValue > 0.31 And SalesweekValue < 0.7 Then
        SalesMonth = 2
    End If
   
    If SalesweekValue > 0.71 And SalesweekValue < 1.1 Then
        SalesMonth = 3
    End If
   
    If SalesweekValue > 1.071 And SalesweekValue < 1.32 Then
        SalesMonth = 4
    End If
   
    If SalesweekValue > 1.34 And SalesweekValue < 1.7 Then
        SalesMonth = 5
    End If
   
    If SalesweekValue > 1.71 And SalesweekValue < 2.01 Then
        SalesMonth = 6
    End If
   
    If SalesweekValue > 2.04 And SalesweekValue < 2.32 Then
        SalesMonth = 7
    End If
   
    If SalesweekValue > 2.33 And SalesweekValue < 2.7 Then
        SalesMonth = 8
    End If
   
    If SalesweekValue > 2.73 And SalesweekValue < 3.01 Then
        SalesMonth = 9
    End If
   
    If SalesweekValue > 3.05 And SalesweekValue < 3.33 Then
        SalesMonth = 10
    End If
   
    If SalesweekValue > 3.35 And SalesweekValue < 3.7 Then
        SalesMonth = 11
    End If
   
    If SalesweekValue > 3.72 And SalesweekValue < 4.3 Then
        SalesMonth = 12
    End If
   
    GetSalesMonth = SalesMonth
 
End Function
 
Public Function GetYear(D1 As Date, GetFiscal As Boolean) As Integer
 
  Dim CurrYear As Integer
   
    If IsDate(D1) = False Then
        Exit Function
    End If
   
    CurrYear = Year(D1)
   
    
    If GetFiscal = True Then
        If GetHalfYear(D1, True) = 1 Then
            CurrYear = CurrYear + 1
        End If
       
        If Month(D1) = 6 Then
            If GetSalesWeek(D1) = 1 Then
                CurrYear = CurrYear + 1
            End If
        End If
    End If
   
    GetYear = CurrYear
   
End Function
Public Function GetSWIM(D1 As Date) As Integer
    Dim Week As Integer
    Dim SWIM As Integer
   
    
    If IsDate(D1) = False Then
        Exit Function
    End If
   
    Week = GetSalesWeek(D1)
   
    If Week <= 4 Then
        SWIM = Week
    End If
    If Week = 5 Then
        SWIM = 1
    End If
    If Week = 6 Then
        SWIM = 2
    End If
    If Week = 7 Then
        SWIM = 3
    End If
    If Week = 8 Then
        SWIM = 4
    End If
    If Week = 9 Then
        SWIM = 5
    End If
    If Week = 10 Then
        SWIM = 1
    End If
    If Week = 11 Then
        SWIM = 2
    End If
    If Week = 12 Then
        SWIM = 3
    End If
    If Week = 13 Then
        SWIM = 4
    End If
    If Week = 14 Then
        SWIM = 1
    End If
    If Week = 15 Then
        SWIM = 2
    End If
    If Week = 16 Then
        SWIM = 3
    End If
    If Week = 17 Then
        SWIM = 4
    End If
    If Week = 18 Then
        SWIM = 1
    End If
    If Week = 19 Then
        SWIM = 2
    End If
    If Week = 20 Then
        SWIM = 3
    End If
    If Week = 21 Then
        SWIM = 4
    End If
    If Week = 22 Then
        SWIM = 5
    End If
    If Week = 23 Then
        SWIM = 1
    End If
    If Week = 24 Then
        SWIM = 2
    End If
    If Week = 25 Then
        SWIM = 3
    End If
    If Week = 26 Then
        SWIM = 4
    End If
    If Week = 27 Then
        SWIM = 1
    End If
    If Week = 28 Then
        SWIM = 2
    End If
    If Week = 29 Then
        SWIM = 3
    End If
    If Week = 30 Then
        SWIM = 4
    End If
    If Week = 31 Then
        SWIM = 1
    End If
    If Week = 32 Then
        SWIM = 2
    End If
    If Week = 33 Then
        SWIM = 3
    End If
    If Week = 34 Then
        SWIM = 4
    End If
    If Week = 35 Then
        SWIM = 5
    End If
    If Week = 36 Then
        SWIM = 1
    End If
    If Week = 37 Then
        SWIM = 2
    End If
    If Week = 38 Then
        SWIM = 3
    End If
    If Week = 39 Then
        SWIM = 4
    End If
    If Week = 40 Then
        SWIM = 1
    End If
    If Week = 41 Then
        SWIM = 2
    End If
    If Week = 42 Then
        SWIM = 3
    End If
    If Week = 43 Then
        SWIM = 4
    End If
    If Week = 44 Then
        SWIM = 1
    End If
    If Week = 45 Then
        SWIM = 2
    End If
    If Week = 46 Then
        SWIM = 3
    End If
    If Week = 47 Then
        SWIM = 4
    End If
    If Week = 48 Then
        SWIM = 5
    End If
    If Week = 49 Then
        SWIM = 1
    End If
    If Week = 50 Then
        SWIM = 2
    End If
    If Week = 51 Then
        SWIM = 3
    End If
    If Week = 52 Then
        SWIM = 4
    End If
   
    If Week = 53 Then
        SWIM = 53
    End If
   
    GetSWIM = SWIM
   
End Function 
 