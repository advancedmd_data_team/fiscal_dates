from setuptools import setup, find_packages

setup(name='fiscal_dates',
      version='0.1',
      description='Sales dates made easy!',
      url='https://github.com/AdvancedMD-Strategy/fiscal_dates',
      author='Nathan Cheever',
      author_email='nathan.cheever@advancedmd.com',
      license=None,
      packages=find_packages(exclude=['tests', '.idea', '.cache', '.ipynb_checkpoints',
      	'__pycache__', 'data', '*.vba', '*.ipynb']),
      zip_safe=False)