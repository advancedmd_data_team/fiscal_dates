#!/usr/env/bin python

"""Utility functions for dealing with sales dates...vectorized!"""

import numpy as np
import pandas as pd
from pandas._libs.tslib import Timestamp
from pandas.api.types import is_datetime64_any_dtype as is_datetime

week_start_day_d = {'friday': 3, 'saturday': 2, 'sunday': 1, 'monday': 0,
                    'tuesday': 6, 'wednesday': 5, 'thursday': 4}


def _is_date_test(col):
    """Test if the value is a date value"""
    if is_datetime(col):
        return col, True
    else:
        print('not a date column')
        return None, False


def _date_part(column, start_day='Saturday', use_system=False):
    """
    A version of VBA's (http://www.excelfunctions.net/vba-datepart-function.html) DatePart function, limited in scope
    to the uses in the original vba code.

    Args:
        date_row: pandas row of a Timestamp
        start_day: str, tells the function when the week should start
        use_system: bool, if True, the first day of the year will be the first week. If the start_day hasn't happened
        yet, previous days in that week will be labeled as week 53.

        For example, in this dataframe, if you ran this function with 'start_day=Friday' and 'use_system=True', then
        it triggers the new week to start on Friday but since it's not the first of the year on rows 4 and 5, they get
        labeled as week 53. Once the new year starts, it returns 1.

                DATE	Day Name	use_system=True	    use_system=False
        0	2016-12-26	Monday	    52	                52
        1	2016-12-27	Tuesday	    52	                52
        2	2016-12-28	Wednesday	52	                52
        3	2016-12-29	Thursday	52	                52
        4	2016-12-30	Friday	    53	                1
        5	2016-12-31	Saturday	53	                1
        6	2017-01-01	Sunday	    1	                1
        7	2017-01-02	Monday	    1	                1
        8	2017-01-03	Tuesday	    1	                1
        9	2017-01-04	Wednesday	1	                1

    Returns:

    """
    column, success = _is_date_test(column)
    if not success:
        return column
    day_shift_value = week_start_day_d.get(start_day.lower())
    if not day_shift_value:
        raise Exception(f"start_day value is not a valid weekday. Please check spelling of {start_day}")
    actual_week = column.dt.week
    start_day = (column.dt.dayofweek + day_shift_value) % 7
    week_value = np.where((0 <= start_day) & (start_day < day_shift_value),
                          np.where(actual_week == 52, 1, actual_week + 1),
                          actual_week)
    week_value = np.where(use_system,
                          np.where((week_value == 1) & (column.dt.month == 12), 53, week_value), week_value)
    return week_value


def get_half_year(column, get_fiscal=False):
    column, success = _is_date_test(column)
    if not success:
        return column
    half_year = np.ceil(column.dt.month / 6)  # rounding up to the nearest int
    half_year = np.where((get_fiscal) & (half_year == 1), 2,
                         np.where(get_fiscal, 1, half_year))
    return half_year


def get_week(column, start_day='Saturday', fiscal=False):
    week = _date_part(column, start_day=start_day)
    if not fiscal:
        return week
    week -= 26
    week = np.where(week <= 0, week + 52, week)
    return week


def get_sales_week(column):
    week = _date_part(column, start_day="Friday", use_system=True)
    week -= 26
    week = np.where(week <= 0, week + 52, week)
    week = np.where(column >= Timestamp('1-1-2016'),
                    np.where(column > Timestamp('6-30-2016'), week, week + 1),
                    # elif
                    np.where(column >= Timestamp('1-1-2021'),
                             np.where(column > Timestamp('7-1-2021'), week, week + 1),
                             # elif
                             np.where(column >= Timestamp('1-1-2027'),
                                      np.where(column > Timestamp('7-1-2027'), week, week + 1),
                                      week
                                      )
                             )
                    )
    return week


def get_qtr(column, get_fiscal=False):
    column, success = _is_date_test(column)
    if not success:
        return column

    quarter = np.ceil(column.dt.month / 3)
    quarter = np.where(get_fiscal,
                       np.where(quarter - 2 <= 0, quarter + 2, quarter - 2),
                       quarter)
    return quarter


def get_sales_qtr(column):
    column, success = _is_date_test(column)
    if not success:
        return column
    sales_week = get_sales_week(column)
    return np.ceil(sales_week / 13)


def get_sales_month(column):
    """TODO: This has some logical issues"""
    column, success = _is_date_test(column)
    if not success:
        return column
    salesweek = get_sales_week(column)
    sales_week_value = salesweek / 13
    sales_month = None
    sales_month = np.where((0 < sales_week_value) & (sales_week_value <= 0.31), 1,
                   np.where((0.31 < sales_week_value) & (sales_week_value <= 0.7), 2,
                    np.where((0.71 < sales_week_value) & (sales_week_value <= 1.071), 3,
                     np.where((1.071 < sales_week_value) & (sales_week_value <= 1.32), 4,
                      np.where((1.32 < sales_week_value) & (sales_week_value <= 1.7), 5,
                       np.where((1.7 < sales_week_value) & (sales_week_value <= 2.01), 6,
                        np.where((2.01 < sales_week_value) & (sales_week_value <= 2.32), 7,
                         np.where((2.32 < sales_week_value) & (sales_week_value <= 2.7), 8,
                          np.where((2.7 < sales_week_value) & (sales_week_value <= 3.01), 9,
                           np.where((3.01 < sales_week_value) & (sales_week_value <= 3.33), 10,
                            np.where((3.33 < sales_week_value) & (sales_week_value <= 3.7), 11,
                             np.where((3.7 < sales_week_value) & (sales_week_value <= 4.3), 12,
                                      sales_month
                           ))))))))))))
    return sales_month


def get_year(column, fiscal=False):
    column, success = _is_date_test(column)
    if not success:
        return column
    current_year = column.dt.year
    if fiscal:
        current_year = np.where(get_half_year(column, get_fiscal=True) == 1, current_year + 1, current_year)
        current_year = np.where((column.dt.month == 6) & (get_sales_week(column) == 1), current_year + 1, current_year)
    return current_year


def get_swim(column):
    column, success = _is_date_test(column)
    if not success:
        return column
    swim = None
    week = pd.Series(get_sales_week(column))
    swim = np.where(week <= 4, week,
            np.where(week.isin([5, 10, 14, 18, 23, 27, 31, 36, 40, 44, 49]), 1,
             np.where(week.isin([6, 11, 15, 19, 24, 28, 32, 37, 41, 45, 50]), 2,
              np.where(week.isin([7, 12, 16, 20, 25, 29, 33, 38, 42, 46, 51]), 3,
               np.where(week.isin([8, 13, 17, 21, 26, 30, 34, 39, 43, 47, 52]), 4,
                np.where(week.isin([9, 22, 35, 48]), 5,
                 np.where(week == 53, 53, swim)))))))
    return swim
