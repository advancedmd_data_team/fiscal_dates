#!/usr/env/bin python

"""Utility functions for dealing with sales dates"""

from dateutil.parser import parse
from pandas._libs.tslib import Timestamp  # for type checking
import numpy as np


week_start_day_d = {'friday': 3, 'saturday': 2, 'sunday': 1, 'monday': 0,
                    'tuesday': 6, 'wednesday': 5, 'thursday': 4}


def _is_date_test(date):
    """Test if the value is a date value"""
    if isinstance(date, Timestamp):
        return date, True
    else:
        print(f"Was not timestamp")
        try:
            date_obj = parse(date)
        except:
            print(f'Unable to parse. Found {date} with type {type(date)}')
            return None, False
        else:
            return date_obj, True


def _date_part(date_row, start_day='Saturday', use_system=False):
    """
    A version of VBA's (http://www.excelfunctions.net/vba-datepart-function.html) DatePart function, limited in scope
    to the uses in the original vba code.

    Args:
        date_row: pandas row of a Timestamp
        start_day: str, tells the function when the week should start
        use_system: bool, if True, the first day of the year will be the first week. If the start_day hasn't happened
        yet, previous days in that week will be labeled as week 53.

        For example, in this dataframe, if you ran this function with 'start_day=Friday' and 'use_system=True', then
        it triggers the new week to start on Friday but since it's not the first of the year on rows 4 and 5, they get
        labeled as week 53. Once the new year starts, it returns 1.

                DATE	Day Name	use_system=True	    use_system=False
        0	2016-12-26	Monday	    52	                52
        1	2016-12-27	Tuesday	    52	                52
        2	2016-12-28	Wednesday	52	                52
        3	2016-12-29	Thursday	52	                52
        4	2016-12-30	Friday	    53	                1
        5	2016-12-31	Saturday	53	                1
        6	2017-01-01	Sunday	    1	                1
        7	2017-01-02	Monday	    1	                1
        8	2017-01-03	Tuesday	    1	                1
        9	2017-01-04	Wednesday	1	                1

    Returns:

    """
    _, success = _is_date_test(date_row)
    if not success:
        return None
    day_shift_value = week_start_day_d.get(start_day.lower())
    if not day_shift_value:
        raise Exception(f"start_day value is not a valid weekday. Please check spelling of {start_day}")
    actual_week = date_row.week
    start_day = (date_row.dayofweek + day_shift_value) % 7
    # the actual week is going to be off between 0 and < day_shift_value, so
    if 0 <= start_day < day_shift_value:
        if actual_week == 52:
            week_value = 1
        else:
            week_value = actual_week + 1
    else:
        week_value = actual_week
    if use_system:
        if week_value == 1 and date_row.month == 12:
            week_value = 53
    return week_value


def get_half_year(date, get_fiscal=False):

    dt, success = _is_date_test(date)
    if not success:
        return None
    else:
        half_year = np.ceil(date.month / 6)  # rounding up to the nearest int

        if get_fiscal and half_year == 1:
            half_year = 2
        elif get_fiscal:
            half_year = 1

        return half_year


def get_week(date_row, start_day='Saturday', fiscal=False) -> int:
    """Calculates the week number based on when the week is considered to start.

    Args:
        date_row: datetime column in an pandas dataframe
        start_day: str, weekday name that we will consider the starting day of the week
        fiscal: bool, if True then will apply a fiscal calculation, default=False

    Returns:
        int
    """
    week = _date_part(date_row, start_day=start_day)
    if fiscal:
        week -= 26
        if week <= 0:
            week += 52

    return week


def get_sales_week(date_row):
    dt, success = _is_date_test(date_row)
    if not success:
        return None
    week = _date_part(date_row, start_day="Friday", use_system=True)
    week -= 26
    if week <= 0:
        week += 52
    if date_row >= parse('1-1-2016'):
        week += 1
        if date_row > parse('6-30-2016'):
            week -= 1
    elif date_row >= parse('1-1-2021'):
        week += 1
        if date_row > parse('7-1-2021'):
            week -= 1
    elif date_row >= parse('1-1-2027'):
        week += 1
        if date_row > parse('7-1-2027'):
            week -= 1

    return week


def get_qtr(date, get_fiscal=False):
    """Finds the quarter
    Args:
        date: date string
        get_fiscal: bool

    Returns:
        int
    """
    dt, success = _is_date_test(date)
    if not success:
        return None
    else:
        quarter = np.ceil(dt.month / 3)

        if get_fiscal:
            quarter -= 2
            if quarter <= 0:
                quarter += 4

        return quarter


def get_sales_qtr(date_row):
    _, success = _is_date_test(date_row)
    if not success:
        return None
    sales_week = get_sales_week(date_row)
    qtr = np.ceil(sales_week / 13)
    return qtr


def get_sales_month(date_row):
    """TODO: this function has some logical issues in the original VBA"""
    _, success = _is_date_test(date_row)
    if not success:
        return None
    salesweek = get_sales_week(date_row)
    sales_week_value = salesweek / 13
    sales_month = None
    if 0 < sales_week_value <= 0.31:
        sales_month = 1
    elif 0.31 < sales_week_value <= 0.7:
        sales_month = 2
    elif 0.7 < sales_week_value <= 1.071:
        sales_month = 3
    elif 1.071 < sales_week_value <= 1.32:
        sales_month = 4
    elif 1.32 < sales_week_value <= 1.7:
        sales_month = 5
    elif 1.7 < sales_week_value <= 2.01:
        sales_month = 6
    elif 2.01 < sales_week_value <= 2.32:
        sales_month = 7
    elif 2.32 < sales_week_value <= 2.7:
        sales_month = 8
    elif 2.7 < sales_week_value <= 3.01:
        sales_month = 9
    elif 3.01 < sales_week_value <= 3.33:
        sales_month = 10
    elif 3.33 < sales_week_value <= 3.7:
        sales_month = 11
    elif 3.7 < sales_week_value <= 4.3:
        sales_month = 12
    else:
        print(f"Missed value of {sales_week_value}")
    return sales_month


def get_year(date_row, fiscal=False):
    """Get the year"""
    _, success = _is_date_test(date_row)
    if not success:
        return None

    current_year = date_row.year
    if fiscal:
        if get_half_year(date_row, True) == 1:
            current_year += 1
        if date_row.month == 6 and get_sales_week(date_row) == 1:
            current_year += 1
    return current_year


def get_swim(date):
    """Get the SWIM, whatever that is(?)"""

    dt, success = _is_date_test(date)
    if not success:
        return None
    else:
        swim = None
        week = get_sales_week(date)

        if week <= 4:
            swim = week
        elif week in [5, 10, 14, 18, 23, 27, 31, 36, 40, 44, 49]:
            swim = 1
        elif week in [6, 11, 15, 19, 24, 28, 32, 37, 41, 45, 50]:
            swim = 2
        elif week in [7, 12, 16, 20, 25, 29, 33, 38, 42, 46, 51]:
            swim = 3
        elif week in [8, 13, 17, 21, 26, 30, 34, 39, 43, 47, 52]:
            swim = 4
        elif week in [9, 22, 35, 48]:
            swim = 5
        elif week == 53:
            swim = 53

    return swim
