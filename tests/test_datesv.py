import pytest

from fiscal_dates.datesv import *
from fiscal_dates.datesv import _is_date_test, _date_part


@pytest.fixture
def data():
    df = pd.read_csv('./data/test_data.csv')
    df.DATE = pd.to_datetime(df.DATE)
    ndf = df[df['DATE'] >= '2016-01-01']
    return ndf


@pytest.fixture
def day_dict():
    return week_start_day_d


def test_is_date_test(data):
    not_a_date_column = data['Day Name']
    date, success = _is_date_test(not_a_date_column)
    assert date is None
    assert not success
    # now with a good date
    date, success = _is_date_test(data['DATE'])
    assert success


def test_date_part(data):
    """Test the date_part function"""
    # Sat is start of week, year option 2
    date_part_sat2 = _date_part(data['DATE'], start_day='Saturday', use_system=False)
    assert all(data['DatePart Sat2'] == date_part_sat2)
    # Friday is start of week, with year option 0
    date_part_fri0 = _date_part(data['DATE'], start_day='Friday', use_system=True)
    assert all(data['DatePart Fri0'] == date_part_fri0)


def test_get_week_start_saturday(data):
    """Test that the get week function is working correctly"""

    py_get_week = get_week(data['DATE'])
    py_get_week_fiscal = get_week(data.DATE, fiscal=True)
    assert all(data['GetWeek'] == py_get_week)
    assert all(data['GetWeek Fiscal'] == py_get_week_fiscal)


def test_get_week_start_friday():
    """Test that the get week function is working correctly"""
    date = pd.Series(pd.Timestamp('2017-01-06'))
    output = get_week(date, start_day='Saturday', fiscal=False)
    assert output[0] == 1
    output = get_week(date, start_day='Saturday', fiscal=True)
    assert output[0] == 27
    # Now test for friday
    output = get_week(date, start_day='Friday', fiscal=False)
    assert output[0] == 2
    output = get_week(date, start_day='Friday', fiscal=True)
    assert output[0] == 28


def test_get_sales_week(data):
    """"""
    py_get_sales_week = get_sales_week(data['DATE'])
    assert all(data['GetSalesWeek'] == py_get_sales_week)


def test_get_half_year(data):
    """tests the get_half_year function"""
    py_get_half_year = get_half_year(data['DATE'])
    assert all(data['GetHalfYear'] == py_get_half_year)
    py_half_year_fiscal = get_half_year(data['DATE'], get_fiscal=True)
    assert all(data['GetHalfYear Fiscal'] == py_half_year_fiscal)


def test_get_qtr(data):
    """tests the get_qtr function"""
    qtr = get_qtr(data['DATE'])
    assert all(data['GetQtr'] == qtr)
    qtr_fiscal = get_qtr(data['DATE'], get_fiscal=True)
    assert all(data['GetQtr Fiscal'] == qtr_fiscal)


def test_get_sales_qtr(data):
    """Tests the get_sales_qtr function"""
    sqtr = get_sales_qtr(data['DATE'])
    assert all(data['GetSalesQtr'] == sqtr)


def test_get_sales_month(data):
    """Test the get_sales_month function"""
    sm = get_sales_month(data['DATE'])
    assert all(data['GetSalesMonth'] == sm)


def test_get_year(data):
    """Test the get_year function"""
    py_get_year = get_year(data['DATE'])
    assert all(data['GetYear'] == py_get_year)
    py_get_year_fiscal = get_year(data['DATE'], fiscal=True)
    assert all(data['GetYear Fiscal'] == py_get_year_fiscal)


def test_get_swim(data):
    """Test the get_swim function"""
    swim = get_swim(data['DATE'])
    assert all(data['SWIM'] == swim)
