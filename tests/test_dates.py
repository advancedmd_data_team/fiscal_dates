import pytest

import datetime
import pandas as pd
from fiscal_dates.dates import *
from fiscal_dates.dates import _is_date_test, _date_part


@pytest.fixture
def data():
#    df = pd.read_csv('./data/FiscalDates.csv')
    df = pd.read_csv('./data/test_data.csv')
    df.DATE = pd.to_datetime(df.DATE)
    ndf = df[df['DATE'] >= '2016-01-01']
    return ndf


@pytest.fixture
def day_dict():
    return week_start_day_d


def test_is_date_test():
    bad_date = "Was this supposed to be a date?"
    date, success = _is_date_test(bad_date)
    assert date is None
    assert not success
    # now with a good date
    good_date = "2018-05-05"
    date, success = _is_date_test(good_date)
    assert isinstance(date, datetime.date)
    assert success


def test_date_part(data):
    """Test the date_part function"""
    # Friday is start of week, with year option 0
    date_part_fri0 = data['DATE'].apply(_date_part, args=('Friday', True))
    assert all(data['DatePart Fri0'] == date_part_fri0)
    # Sat is start of week, year option 2
    date_part_sat2 = data['DATE'].apply(_date_part, args=('Saturday', False))
    assert all(data['DatePart Sat2'] == date_part_sat2)


def test_get_week_start_saturday(data):
    """Test that the get week function is working correctly"""
    # date_string = '2017-12-04'
    # date = pd.to_datetime(date_string)
    py_get_week = data['DATE'].apply(get_week)
    py_get_week_fiscal = data['DATE'].apply(get_week, args=('Saturday', True))
    assert (data['GetWeek'] == py_get_week).all()
    assert all(data['GetWeek Fiscal'] == py_get_week_fiscal)


def test_get_week_start_friday():
    """Test that the get week function is working correctly"""
    date_string = '2017-01-06'
    date = pd.to_datetime(date_string)
    output = get_week(date, start_day='Saturday', fiscal=False)
    assert output == 1
    output = get_week(date, start_day='Saturday', fiscal=True)
    assert output == 27
    # Now test for friday
    output = get_week(date, start_day='Friday', fiscal=False)
    assert output == 2
    output = get_week(date, start_day='Friday', fiscal=True)
    assert output == 28


def test_get_sales_week(data):
    """"""
    py_get_sales_week = data['DATE'].apply(get_sales_week)
    assert all(data['GetSalesWeek'] == py_get_sales_week)


def test_get_half_year(data):
    """tests the get_half_year function"""
    py_get_half_year = data['DATE'].apply(get_half_year)
    assert all(data['GetHalfYear'] == py_get_half_year)
    py_half_year_fiscal = data['DATE'].apply(get_half_year, args=(True,))
    assert all(data['GetHalfYear Fiscal'] == py_half_year_fiscal)


def test_get_qtr(data):
    """tests the get_qtr function"""
    qtr = data['DATE'].apply(get_qtr)
    assert all(data['GetQtr'] == qtr)
    qtr_fiscal = data['DATE'].apply(get_qtr, args=(True,))
    assert all(data['GetQtr Fiscal'] == qtr_fiscal)


def test_get_sales_qtr(data):
    """Tests the get_sales_qtr function"""
    sqtr = data['DATE'].apply(get_sales_qtr)
    assert all(data['GetSalesQtr'] == sqtr)


def test_get_sales_month(data):
    """Test the get_sales_month function"""
    sm = data['DATE'].apply(get_sales_month)
    assert all(data['GetSalesMonth'] == sm)


def test_get_year(data):
    """Test the get_year function"""
    py_get_year = data['DATE'].apply(get_year)
    assert all(data['GetYear'] == py_get_year)
    py_get_year_fiscal = data['DATE'].apply(get_year, args=(True,))
    assert all(data['GetYear Fiscal'] == py_get_year_fiscal)


def test_get_swim(data):
    """Test the get_swim function"""
    swim = data['DATE'].apply(get_swim)
    assert all(data['SWIM'] == swim)

